'use strict';

const API_BASE = 'virtserver.swaggerhub.com/LAB4_Final/LAB4_Final/1'

const baseGetHandler = (req, res) => {
    res.redirect('/login');
};

const loginGetHandler = (req, res) => {
    res.render('login');
};

const loginPostHandler = (req, res) => {
    (() => {})();
};

const registerGetHandler = (req, res) => {
    res.render('register');
};

const registerPostHandler = (req, res) => {
    (() => {})();
};

const homeGetHandler = (req, res) => {
    res.render('home');
};

const createEventGetHandler = (req, res) => {
    res.render('create-event');
};

const eventGetHandler = (req, res) => {
    res.render('event');
};

const createListGetHandler = (req, res) => {
    res.render('create-list');
};

const listGetHandler = (req, res) => {
    res.render('list');
};

const createLocationGetHandler = (req, res) => {
    res.render('create-location');
};

module.exports = {
    getBase: baseGetHandler,
    getLogin: loginGetHandler,
    postLogin: loginPostHandler,
    getRegister: registerGetHandler,
    postRegister: registerPostHandler,
    getHome: homeGetHandler,
    getCreateEvent: createEventGetHandler,
    getEvent: eventGetHandler,
    getCreateList: createListGetHandler,
    getList: listGetHandler,
    getCreateLocation: createLocationGetHandler
}