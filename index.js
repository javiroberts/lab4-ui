'use strict';

const express = require('express');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');

const handlers = require('./handlers');

const app = express();

// view engine setup
const hbsOpts = {
    defaultLayout: 'main',
    extname: 'handlebars',
    layoutsDir: `${__dirname}/views/layouts/`,
    partialsDir: `${__dirname}/views/partials/`
}
const hbs = exphbs.create(hbsOpts);
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

// basic app setup
app.use(bodyParser.urlencoded({ extended: true }));

// route mapping
app.get('/', handlers.getBase);

app.get('/login', handlers.getLogin);
app.post('/login', handlers.postLogin);

app.get('/register', handlers.getRegister);
app.post('/register', handlers.postRegister);

app.get('/home', handlers.getHome);

app.get('/create-event', handlers.getCreateEvent);

app.get('/event', handlers.getEvent);

app.get('/create-list', handlers.getCreateList);

app.get('/list', handlers.getList);

app.get('/create-location', handlers.getCreateLocation);

// startup sequence
const server = app.listen(4002, '0.0.0.0', () => {
    const host = server.address().address;
    const port = server.address().port;
    console.log('server is now listening on http://%s:%s', host, port);
})